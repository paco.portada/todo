<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewEmail extends Mailable
{
    use Queueable, SerializesModels;
    public $from;
    public $subject;
    public $message;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(String $from, String $subject, String $message)
    {
		$this->from = $from;
        $this->subject = $subject;
        $this->message = $message;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //return $this->view('view.name');
        return $this->view('emails.send');
                    //->with('message', $this->message);
                    //->from($this->from)
                    //->from('yo@alumno.xyz')
                    //->subject($this->subject);
    }
}
